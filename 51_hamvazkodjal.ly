\version "2.22.0"

#(set-default-paper-size "a4landscape")

\header {
  title = "51. Hamvazkodjál hívő lélek"
}

 \markup {
    \vspace #2
}

global = {
  \key g \minor
  \time 4/4
}

sopMusic = \relative c' {
  c4 g'8 ( as) bes4 g | f8 es d4 c2 | c'4 c bes d | c8 bes a4 g2 |
  bes4 g g g | f8 es d4 c2 | bes'4 g g g | f8 es d4 c2 \bar "|."
} 

altoMusic = \relative c' {
  c4 d es2 | c4 bes8 a g2 | f'4 g f g | g f8 es d2 |
  f4 es d c | c4. bes8 a2 | bes4 c d c | c c8 b c2 |
}

tenorMusic = \relative c {
  es4 bes'2. | a8 g f4 es2 | a4 g8 a d2 | es8 d c4 bes2 |
  bes2. es,4 | f1 | d2. e4| a d,8 g es2 |
}

bassMusic = \relative c {
  c4 bes g es | f bes c2 | f4 es d bes | c f g2 |
  d4 es bes c | a bes f2 | g4 a bes c | f, g c2
}

verseOne = \lyricmode {
  Ham -- vaz -- kod -- jál, hí -- vő lé -- lek,
  Mert por -- ba tér ez az é -- let.
  A -- kár -- meny -- nyit ör -- ven -- dez -- nél.
  Ne fe -- ledd, hogy por -- ból let -- tél.
}

verseTwo = \lyricmode {
  Ham -- vaz -- kod -- jál, te dús -- gaz -- dag:
  Mert ja -- va -- id cser -- ben hagy -- nak.
  É -- vek száll -- nak, és ma -- hol -- nap
  Át -- a -- dod mind föl -- di por -- nak.
}

verseThree = \lyricmode {
  Ham -- vaz -- kod -- jál, sze -- gény em -- ber,
  Vi -- seld sor -- sod tü -- re -- lem -- mel:
  Ha -- mu hull a gyöt -- re -- lem -- re
  S_ir -- ga -- lom a hű szí -- vek -- re.
}

verseFour = \lyricmode {
  Ö -- rö -- me -- ink mind el -- múl -- nak,
  Köny -- nye -- ink is por -- ba hull -- nak.
  Bol -- do -- gok, kik meg -- ja -- vul -- nak,
  Ha -- mu sza -- ván meg -- in -- dul -- nak.
}

\score {
  \new PianoStaff <<
    \new Lyrics = sopranos
    \new Staff = women <<
      \new Voice = "sopranos" {
        \voiceOne
        \global \sopMusic
      }
      \new Voice = "altos" {
        \voiceTwo
        \global \altoMusic
      }
    >>
    \new Lyrics = "altos"
    \new Lyrics = "tenors"
    \new Staff = men <<
      \clef bass
      \new Voice = "tenors" {
        \voiceOne
        \global \tenorMusic
      }
      \new Voice = "basses" {
        \voiceTwo  \global \bassMusic
      }
    >>
    \new Lyrics = basses
    %\context Lyrics = sopranos \lyricsto sopranos \words
    \context Lyrics = sopranos \lyricsto sopranos \verseOne
    \context Lyrics = altos \lyricsto sopranos \verseTwo
    \context Lyrics = tenors \lyricsto sopranos \verseThree
    \context Lyrics = basses \lyricsto sopranos \verseFour
    %\context Lyrics = basses \lyricsto basses \words
  >>
  \layout {
    \context {
      \Lyrics
      \override VerticalAxisGroup.staff-affinity = ##f
      \override VerticalAxisGroup.staff-staff-spacing =
        #'((basic-distance . 0)
	   (minimum-distance . 2)
	   (padding . 2))
    }
    \context {
      \Staff
      \override VerticalAxisGroup.staff-staff-spacing =
        #'((basic-distance . 0)
	   (minimum-distance . 2)
	   (padding . 2))
    }
		\context {
			\Score
			\override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/16)
		}
  }
}

