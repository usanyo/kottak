\version "2.22.0"

#(set-default-paper-size "a4landscape")

\header {
  title = "77. Megváltó királyunk"
}

 \markup {
    \vspace #2
}

global = {
  \key d \major
  \time 4/4
}

sopMusic = \relative c' {
  % VERSE ONE
  a'4 b8( a) g4 b | (a g) fis2 | a4 a8( g) fis4 e | d1 |
  a'4 b8( a) g4 b | (a g) fis2 | a4 a8( g) fis4 e | d1 |
  e4 e a2 | g4 fis e2 | e4 a g fis | e1 |
  a4 b8( a) g4 b | (a g) fis2 | a4 a8( g) fis4 e | d1 \bar "|."
} 

altoMusic = \relative c' {
  % VERSE ONE
  d2 b4 cis | d e d2 | e4 cis d cis | d1 |
  cis4 d2 e4 | fis e8 d cis2 | d cis | d1 |
  c4 b e dis | e dis e2 | e4 dis e dis | e c b2 |
  fis'4 e d2 | cis d | d4 e d cis | d1 |
}

tenorMusic = \relative c' {
  fis,2 g4 e | a b8 cis d2 | a2. g4 | fis1 |
  e4 g2. | fis4 b ais2 | a4 b a g | fis1 |
  fis4 g c b | b a g2 | b4 a b a | a g8 fis g2 |
  a4 e8 fis g2 | e d4  d' | c b a g | fis1 |
}

bassMusic = \relative c {
  d4 b e a, | fis g8 a b2 | cis4 e d a | d1 |
  a4 g b cis | d e fis2 | fis,4 g a ais | b1 |
  a4 g fis fis' | e b c2 | g'4 fis e b | e1 |
  d4 c b g | a ais b2 | fis'4 g a a, | d1 |
}

verseOne = \lyricmode {
  Meg -- vál -- tó ki -- rá -- lyunk e -- lé -- be me -- gyünk,
  Mél -- tó tisz -- te -- let -- tel U -- runk -- hoz le -- gyünk,
  Ho -- zsan -- nát ki -- ált -- son né -- ki min -- den hív,
  Dí -- csé -- ret -- tel ál -- dást mond -- jon nyelv és szív.
}

verseTwo = \lyricmode {
  Így tisz -- tel -- te Jé -- zust a zsi -- dó nem -- zet,
  Vi -- rág -- va -- sár -- nap -- ján nagy gyü -- le -- ke -- zet,
  E -- lé -- be ki -- men -- tek pál -- ma -- á -- gak -- kal.
  Di -- csé -- rő é -- nek -- kel és vi -- rá -- gok -- kal.
}

verseThree = \lyricmode {
  Hogy a szent vá -- ros -- hoz már kö -- zel -- ge -- tett,
  És fé -- nyes pom -- pá -- val bé -- ve -- zet -- te -- tett,
  Ru -- há -- ját a nép -- ség föld -- re te -- rí -- té,
  És út -- ját zöld ág -- gal föl -- é -- ke -- sí -- té.
}

verseFour = \lyricmode {
  És ör -- ven -- dő szív -- vel ki -- ál -- to -- zá -- nak:
  Ho -- zsán -- na, ho -- zsán -- na Dá -- vid fi -- á -- nak!
  Ál -- dott az, ki az Úr szent ne -- vé -- ben jön!
  Áld -- ják őt a hí -- vek most és ö -- rök -- kön.
}

\score {
  \new PianoStaff <<
    \new Lyrics = sopranos
    \new Staff = women <<
      \new Voice = "sopranos" {
        \voiceOne
        \global \sopMusic
      }
      \new Voice = "altos" {
        \voiceTwo
        \global \altoMusic
      }
    >>
    \new Lyrics = "altos"
    \new Lyrics = "tenors"
    \new Staff = men <<
      \clef bass
      \new Voice = "tenors" {
        \voiceOne
        \global \tenorMusic
      }
      \new Voice = "basses" {
        \voiceTwo  \global \bassMusic
      }
    >>
    \new Lyrics = basses
    %\context Lyrics = sopranos \lyricsto sopranos \words
    \context Lyrics = sopranos \lyricsto sopranos \verseOne
    \context Lyrics = altos \lyricsto sopranos \verseTwo
    \context Lyrics = tenors \lyricsto sopranos \verseThree
    \context Lyrics = basses \lyricsto sopranos \verseFour
    %\context Lyrics = basses \lyricsto basses \words
  >>
  \layout {
    \context {
      \Lyrics
      \override VerticalAxisGroup.staff-affinity = ##f
      \override VerticalAxisGroup.staff-staff-spacing =
        #'((basic-distance . 0)
	   (minimum-distance . 2)
	   (padding . 2))
    }
    \context {
      \Staff
      \override VerticalAxisGroup.staff-staff-spacing =
        #'((basic-distance . 0)
	   (minimum-distance . 2)
	   (padding . 2))
    }
		\context {
			\Score
			\override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/16)
		}
  }
}

