\header {
  title = "ADESTE FIDELES"
  subtitle = "A solesi bencések dallamaiból"
	instrument = "Babits Mihály fordításával"
  composer = "DEMÉNY DEZSŐ"
}

global = {
  \key g \major
  \time 2/2
}

sopMusic = \relative c' {
  % VERSE ONE
  g' g2 d4 | g a8 fis d2 | b'4 a b c | b2 a | g4 g2 fis4 |
  e fis8 g a2 | b4 g fis e | d1
	\repeat volta 1 {
		d'2 c | b4 c2 b4 | a b g a | g fis8 e d2 |
		g4 g2 d4 | g a g d | b' b2 a4 | b c b a |
		b c2 b4 | a g fis2 | \tuplet 3/2 {g4 a b} c4\fermata r\fermata | b2 a4. a8 | g1 |
	}
}

altoMusic = \relative c' {
  % VERSE ONE
  d4 d2 d4 | d cis d2 | g4 fis g g | g2 fis | e4 e2 d4 |
	c c8( d) d2 | d4( e) d cis | d1
	\repeat volta 1 {
		g2 g | g4 g2 g4 | fis fis e e | e8( d cis4) d2 |
		d4 d2 d4 | d d d d | g g2 fis4 | g g g e |
		g g2 g4 | e e dis2 | e2( e4) r | g2( e4) fis | d1 | 
	}
}

tenorMusic = \relative c' {
	b4 b2 a4 | b a8( g) fis2 | d'4 d d e | d2 d | b4 b2 b4 |
	g a8( g) fis2 | g4 b a( g) | fis1
	\repeat volta 1 {
		d'2 e | d4 e2 d4 | d d b a | b( a8 g) fis2 |
		b4 c( b) a | b c b a | d e( d) c | d c d c |
		d e2 e4 | c b b2 | \tuplet 3/2 {b4( a g} a4\fermata) r\fermata | b( d cis) c | b1 |
	}
}

bassMusic = \relative c {
	g'4 g2 fis4 | e a, d2 | g,4 d' b a | b( c) d2 | e4 e2 b4 |
	c a8 b d2 | g, a4 a | d1
	\repeat volta 1 {
		b2 c | g'4 e2 g4 | d b e cis | e( a,) d2 |
		g,4 g'2 g4 | g g g g | g g2 g4 | g e g a |
		g c,2 e4 | fis g8( a) b2 | \tuplet 3/2 {e,4 c b} a4 r | d2. d4 | g,1 |
	} 
}

latin = \lyricmode {
  Ad -- es -- te fi -- de -- les,
	lae -- ti tri -- um -- phan -- tes:
	Ve -- ni -- te, ve -- ni -- te in Beth -- le -- hem!
	Nat -- tum vi -- de -- te Re -- gem an -- ge -- lo -- rum:
	Ve -- ni -- te a -- do -- re -- mus,
	ve -- ni -- te a -- do -- re -- mus,
	ve -- ni -- te a -- do -- re -- mus Do -- mi -- num!
}

hungarian = \lyricmode {
	\override Lyrics.LyricText.font-shape = #'italic
	Jöj -- je -- tek ó hí -- vek
	di -- a -- dal -- mas -- kod -- va.
	Jöj -- je -- tek, jöj -- je -- tek Bet -- le -- hem -- be!
	Meg -- szü -- le -- tett az an -- gya -- lok ki -- rá -- lya.
	Ó jöj -- je -- tek i -- mád -- juk,
	ó jöj -- je -- tek i -- mád -- juk,
	ó jöj -- je -- tek i -- mád -- juk az U -- rat!
}

\score {
  \new StaffGroup <<
    \new Lyrics = sopranos
    \new Staff = women <<
      \new Voice = "sopranos" {
        \voiceOne
        \global \sopMusic
      }
      \new Voice = "altos" {
        \voiceTwo
        \global \altoMusic
      }
    >>
    \new Lyrics = "altos"
    \new Lyrics = "tenors"
    \new Staff = men <<
      \clef bass
      \new Voice = "tenors" {
        \voiceOne
        \global \tenorMusic
      }
      \new Voice = "basses" {
        \voiceTwo  \global \bassMusic
      }
    >>
    \new Lyrics = basses
    %\context Lyrics = sopranos \lyricsto sopranos \words
    \context Lyrics = altos \lyricsto altos \latin
    \context Lyrics = tenors \lyricsto tenors \hungarian
    %\context Lyrics = basses \lyricsto basses \words
  >>
  \layout {
    \context {
      \Lyrics
      \override VerticalAxisGroup.staff-affinity = ##f
      \override VerticalAxisGroup.staff-staff-spacing =
        #'((basic-distance . 0)
	   (minimum-distance . 2)
	   (padding . 2))
    }
    \context {
      \Staff
      \override VerticalAxisGroup.staff-staff-spacing =
        #'((basic-distance . 0)
	   (minimum-distance . 2)
	   (padding . 2))
    }
		\context {
			\Score
			\override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/16)
		}
  }
}

